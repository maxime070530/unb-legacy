package webservice.api;

import java.net.URISyntaxException;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.ws.rs.core.Response;

@WebService
@SOAPBinding(style = Style.RPC)
public interface IBadisteWS {

	Response findAllJoueursByClub() throws URISyntaxException;

}
