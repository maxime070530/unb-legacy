
package fr.max.lvdc.bean.moduleInscription;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;

@Component("beanListeTournois")
@Scope("view")
@Lazy(true)
public class BeanListeTournois extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = -8662031353350236371L;

    @SuppressWarnings("unused")
    /** The Constant LOGGER. */
    private static final Logger LOGGER =
	    Logger.getLogger(BeanListeTournois.class);

    // debut injection
    @Autowired
    private ICommunFacade communFacade;

    private List<TournoiPOJO> listetournois = null;
    // fin injection
    private TournoiPOJO selectedTournoi = null;

    @PostConstruct
    public void init() {
	this.listetournois = this.communFacade.findAllTournoi();
	// this.listetournois = this.communFacade.findAllTournoiByDate();
    }

    public List<TournoiPOJO> getListetournois() {
	return listetournois;
    }

    public void setListetournois(List<TournoiPOJO> listetournois) {
	this.listetournois = listetournois;
    }

    public TournoiPOJO getSelectedTournoi() {
	return selectedTournoi;
    }

    public void setSelectedTournoi(TournoiPOJO selectedTournoi) {
	this.selectedTournoi = selectedTournoi;
    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowSelect(final SelectEvent event) {

	this.selectedTournoi = ((TournoiPOJO) event.getObject());

    }

    /**
     * @param event
     *            SelectEvent
     */
    public void onRowUnselect(final UnselectEvent event) {

	this.selectedTournoi = null;

    }

}
