
package fr.max.lvdc.bean.moduleInscription;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.CloseEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.io.Files;

import fr.max.lvdc.bean.AbstractGenericBean;
import fr.max.lvdc.core.GetParameterUrl;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IExportFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@Component("beanTournoi")
@Scope("view")
@Lazy(true)
public class BeanTournoi extends AbstractGenericBean {

    /**
     * 
     */
    private static final long serialVersionUID = 4555595834278269723L;

    /** The Constant LOGGER. */
    @SuppressWarnings("unused")
    private static final Logger LOGGER = Logger.getLogger(BeanTournoi.class);

    // injection
    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userManagerFacade;

    @Autowired
    private IExportFacade exportFacade;

    // injection

    private TournoiPOJO tournoi = null;
    private JoueurPOJO joueur = null;

    private StreamedContent fileExport;

    private List<InscriptionPOJO> listeInscriptionsModifie =
	    new ArrayList<InscriptionPOJO>();

    private InscriptionPOJO inscription = new InscriptionPOJO();

    @GetParameterUrl(paramName = "idTournoi")
    private String idTournoi;

    private List<ClubPOJO> listeClubs = null;
    private ClubPOJO club = new ClubPOJO();

    private List<DocumentPOJO> listeDocuments = null;
    private List<VeteranPOJO> listeCategorieVeterans = null;

    private UploadedFile filer = null;
    private DocumentPOJO fileToUpload = new DocumentPOJO();

    private RolePOJO role = null;
    private UserPOJO user = null;
    private boolean verifCheck = false;

    private InscriptionPOJO selectedInscription;

    @PostConstruct
    public void init() {

	this.listeClubs = this.communFacade.findAllClub();

	if (idTournoi != null) {
	    this.tournoi = this.communFacade
		    .findTournoi(Integer.parseInt(idTournoi));

	    for (InscriptionPOJO inscription : this.tournoi
		    .getListeInscriptions()) {
		if (inscription.getJoueur()
			.equals(getUserAuthenticate().getJoueur())) {
		    this.selectedInscription = inscription;
		}
	    }

	} else {
	    this.tournoi = new TournoiPOJO();
	    this.tournoi.setActif(false);
	    setModificationGeneral(true);

	}

    }

    public InscriptionPOJO getSelectedInscription() {
	return selectedInscription;
    }

    public void setSelectedInscription(InscriptionPOJO selectedInscription) {
	this.selectedInscription = selectedInscription;
    }

    public List<DocumentPOJO> getListeDocuments() {
	return listeDocuments;
    }

    public UploadedFile getFiler() {
	return filer;
    }

    public void setFiler(UploadedFile filer) {
	this.filer = filer;
    }

    public UserPOJO getUser() {
	return user;
    }

    public void setUser(UserPOJO user) {
	this.user = user;
    }

    public IUserManagerFacade getUserManagerFacade() {
	return userManagerFacade;
    }

    public void setUserManagerFacade(IUserManagerFacade userManagerFacade) {
	this.userManagerFacade = userManagerFacade;
    }

    public List<VeteranPOJO> getListeCategorieVeterans() {
	return listeCategorieVeterans;
    }

    public TournoiPOJO getTournoi() {
	return tournoi;
    }

    public void setTournoi(TournoiPOJO tournoi) {
	this.tournoi = tournoi;
    }

    public StreamedContent getFileExport() {
	return fileExport;
    }

    public void setFileExport(StreamedContent fileExport) {
	this.fileExport = fileExport;
    }

    public List<InscriptionPOJO> getListeInscriptionsModifie() {
	return listeInscriptionsModifie;
    }

    public void setListeInscriptionsModifie(
	    List<InscriptionPOJO> listeInscriptionsModifie) {
	this.listeInscriptionsModifie = listeInscriptionsModifie;
    }

    public RolePOJO getRole() {
	return role;
    }

    public void setRole(RolePOJO role) {
	this.role = role;
    }

    public JoueurPOJO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurPOJO joueur) {
	this.joueur = joueur;
    }

    public InscriptionPOJO getInscription() {
	return inscription;
    }

    public void setInscription(InscriptionPOJO inscription) {
	this.inscription = inscription;
    }

    public List<ClubPOJO> getListeClubs() {
	return listeClubs;
    }

    public ClubPOJO getClub() {
	return club;
    }

    public void setClub(ClubPOJO club) {
	this.club = club;
    }

    public DocumentPOJO getFileToUpload() {
	return fileToUpload;
    }

    public void setFileToUpload(DocumentPOJO fileToUpload) {
	this.fileToUpload = fileToUpload;
    }

    public boolean isVerifCheck() {
	return verifCheck;
    }

    public void setVerifCheck(boolean verifCheck) {
	this.verifCheck = verifCheck;
    }

    /**
     * 
     */
    public void upload() {

	if (filer != null) {

	    final FacesMessage message = new FacesMessage("Succesful",
		    filer.getFileName() + " is uploaded.");
	    FacesContext.getCurrentInstance().addMessage(null, message);

	    if (fileToUpload.getNom() == null
		    || fileToUpload.getNom().isEmpty()) {
		fileToUpload.setNom(filer.getFileName());
	    }

	    fileToUpload.setTaille(filer.getContents().length);
	    fileToUpload.setMime(filer.getContentType());
	    fileToUpload.setExtension(filer.getFileName()
		    .substring(filer.getFileName().lastIndexOf(".") + 1));
	    fileToUpload.setData(filer.getContents());

	    this.communFacade.saveDocument(fileToUpload);
	    this.tournoi.getListeDocument().add(fileToUpload);
	    this.communFacade.saveTournoi(tournoi);

	    this.fileToUpload = null;
	    this.filer = null;

	}
    }

    /**
     * @param fichier
     *            FichierEntitePOJO
     * @return StreamedContent download
     */
    public StreamedContent download(DocumentPOJO document) {
	StreamedContent file;

	final InputStream stream =
		new ByteArrayInputStream(document.getData());
	file = new DefaultStreamedContent(stream, document.getMime(),
		document.getFullName());
	return file;
    }

    /**
     * 
     */
    public void save() {

	try {

	    for (final InscriptionPOJO currentInscription : listeInscriptionsModifie) {
		try {
		    this.communFacade.deleteInscription(currentInscription);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }

	    if (this.tournoi.getLien().equals("")
		    || this.tournoi.getLien() == null) {
		this.tournoi.setLien("aaa");
	    }

	    this.communFacade.saveTournoi(this.tournoi);

	    setModificationGeneral(false);
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage("Sauvegarde effectu�e du tournoi", " "));

	} catch (final Exception e) {

	    // message d erreur
	    FacesContext.getCurrentInstance().addMessage(null,
		    new FacesMessage(FacesMessage.SEVERITY_ERROR,
			    e.getMessage(), " erreur save"));
	}

    }

    /**
     * 
     */

    public void annuler() {

	setModificationGeneral(false);
	this.tournoi =
		this.communFacade.findTournoi(Integer.parseInt(idTournoi));

    }

    public String delete() {

	this.communFacade.deleteTournoi(this.tournoi);

	FacesContext.getCurrentInstance().addMessage(null,
		new FacesMessage("Suppression du tournoi ", " "));

	return "LISTE_TOURNOIS";

    }

    /**
     * @param fichier
     *            FichierEntitePOJO
     */
    public void supprimer(final DocumentPOJO fichier) {

	if (fichier != null) {

	    this.tournoi.getListeDocument().remove(fichier);
	    this.communFacade.deleteDocument(fichier);

	    final FacesMessage msg = new FacesMessage(
		    FacesMessage.SEVERITY_INFO, "Document supprim�", "");
	    FacesContext.getCurrentInstance().addMessage(null, msg);
	}
    }

    public void initDialogDocument(final CloseEvent event) {

	this.filer = null;
	this.fileToUpload = null;
	resetInputValues(event.getComponent().getChildren());

    }

    /**
     * @param event
     *            AjaxBehaviorEvent
     */
    public void openDialogDocument(final AjaxBehaviorEvent event) {

	fileToUpload = new DocumentPOJO();

    }

    public void removeInscription(final InscriptionPOJO inscription) {
	this.listeInscriptionsModifie.add(inscription);
	this.tournoi.getListeInscriptions().remove(inscription);
    }

    @Override
    public String getLibelleHeader() {
	return super.getLibelleHeader("Tournoi",
		this.tournoi.getLibelleTournoi());
    }

    public void exporter() throws IOException {

	if (this.tournoi.getCategorieTournoi().isCategorieSenior()) {

	    final File export = this.exportFacade.exportInscriptions(
		    this.tournoi.getListeInscriptions(), this.tournoi);

	    final InputStream stream =
		    Files.asByteSource(export).openStream();
	    this.fileExport = new DefaultStreamedContent(stream,
		    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
		    "export_inscription_" + this.tournoi.getLibelleTournoi()
			    + ".xlsx");

	}

    }

    public void onSeniorStatusChange() {
	if (this.tournoi.getCategorieTournoi().isCategorieSenior() == true)
	    this.tournoi.getCategorieTournoi().setCategorieVeteran(false);
	;
    }

    public void onVeteranStatusChange() {
	if (this.tournoi.getCategorieTournoi().isCategorieVeteran() == true)
	    this.tournoi.getCategorieTournoi().setCategorieSenior(false);
    }

}
