
package fr.max.lvdc.core;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CacheFilter implements Filter {

    /**
     * The Constant NBDAY.
     */
    private static final long NBDAY = 30;

    /**
     * The Constant DAYTOSECOND.
     */
    private static final long DAYTOSECOND = 86400;

    /**
     * The Constant maxAge.
     */
    private static long maxAge = DAYTOSECOND * NBDAY;

    @Override
    public final void doFilter(final ServletRequest request,
	    final ServletResponse response, final FilterChain chain)
	    throws IOException, ServletException {
	HttpServletResponse httpResponse = (HttpServletResponse) response;
	String uri = ((HttpServletRequest) request).getRequestURI();
	if (uri.contains(".js") || uri.contains(".css")
		|| uri.contains(".svg") || uri.contains(".gif")
		|| uri.contains(".woff") || uri.contains(".png")) {
	    // TODO : faire �a que si on es en environement production
	    httpResponse.setHeader("Cache-Control", "max-age=" + maxAge);
	    // httpResponse.addHeader("Cache-Control", "max-age=" + maxAge);
	}
	chain.doFilter(request, response);
    }

    @Override
    public final void init(final FilterConfig filterConfig)
	    throws ServletException {
	System.out.println("Cache Filter started: ");
    }

    @Override
    public void destroy() {
    }
}
