package fr.max.lvdc.core;

public class ConstantGlobal {

	public static final String SYMBOLE_EURO = "\u20AC";

	public static final String URL_COMMUNE_DATA_NOVA = "https://datanova.legroupe.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=json&timezone=Europe/Berlin";

	public static final String FACES_REQUEST_HEADER = "faces-request";

}
