package fr.max.lvdc.core;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.util.StringUtils;

public class MyInvalidSessionStrategy implements InvalidSessionStrategy {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private String invalidSessionUrl;

	@Override
	public void onInvalidSessionDetected(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException,
			ServletException {

		boolean ajaxRedirect = "partial/ajax".equals(request
				.getHeader(ConstantGlobal.FACES_REQUEST_HEADER));
		if (ajaxRedirect) {
			String contextPath = request.getContextPath();
			String redirectUrl = contextPath + invalidSessionUrl;
			logger.debug(
					"Session expired due to ajax request, redirecting to '{}'",
					redirectUrl);

			String ajaxRedirectXml = Utils.createAjaxRedirectXml(redirectUrl);
			logger.debug("Ajax partial response to redirect: {}",
					ajaxRedirectXml);

			response.setContentType("text/xml");
			response.getWriter().write(ajaxRedirectXml);
		} else {
			String requestURI = getRequestUrl(request);
			logger.debug(
					"Session expired due to non-ajax request, starting a new session and redirect to requested url '{}'",
					requestURI);
			request.getSession(true);
			response.sendRedirect(requestURI);
		}

	}

	private String getRequestUrl(final HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();

		String queryString = request.getQueryString();
		if (StringUtils.hasText(queryString)) {
			requestURL.append("?").append(queryString);
		}

		return requestURL.toString();
	}

	public void setInvalidSessionUrl(final String invalidSessionUrl) {
		this.invalidSessionUrl = invalidSessionUrl;
	}

}
