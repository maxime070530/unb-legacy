
package fr.max.lvdc.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Utils {

    public static String formatDateTexteFR(final Date date) {
	final SimpleDateFormat sdf =
		new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
	if (date != null) {
	    return sdf.format(date);
	}
	return null;
    }

    public static String formatDateHeureTexteFR(final Date date) {
	final SimpleDateFormat sdf =
		new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.FRANCE);
	if (date != null) {
	    return sdf.format(date);
	}
	return null;
    }

    public static String formatDateInFileName(final Date date) {
	final SimpleDateFormat sdf =
		new SimpleDateFormat("dd_MM_yyyy", Locale.FRANCE);
	if (date != null) {
	    return sdf.format(date);
	}
	return null;
    }

    public static String formatDateString(final Date date) {
	final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	if (date != null) {
	    return sdf.format(date);
	}
	return null;
    }

    public static String formatDateHeureString(final Date date) {
	final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	if (date != null) {
	    return sdf.format(date);
	}
	return null;
    }

    public static String buildMessage(final String template,
	    final Object invokedObject) {

	ParserContext parserContext = new TemplateParserContext();
	ExpressionParser expressionParser = new SpelExpressionParser();

	try {
	    Expression expression =
		    expressionParser.parseExpression(template, parserContext);

	    String evaluatedMessage =
		    expression.getValue(invokedObject, String.class);

	    return evaluatedMessage;

	} catch (RuntimeException e) {
	    // erreur
	    e.printStackTrace();
	}
	return null;
    }

    public static Object resolveObject(final String template,
	    final Object invokedObject) {

	ParserContext parserContext = new TemplateParserContext();
	ExpressionParser expressionParser = new SpelExpressionParser();

	try {
	    Expression expression =
		    expressionParser.parseExpression(template, parserContext);

	    Object evaluatedObject =
		    expression.getValue(invokedObject, Object.class);

	    return evaluatedObject;

	} catch (RuntimeException e) {
	    // erreur
	    e.printStackTrace();
	}
	return null;
    }

    public static String createAjaxRedirectXml(final String redirectUrl) {
	return new StringBuilder()
		.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
		.append("<partial-response><redirect url=\"")
		.append(redirectUrl)
		.append("\"></redirect></partial-response>").toString();
    }

    public static String encodeBCrypt(final String value) {

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	return encoder.encode(value);

    }

    public static boolean matchBCrypt(final String value,
	    final String encode) {

	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	return encoder.matches(value, encode);
    }
}
