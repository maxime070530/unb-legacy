package fr.max.lvdc.core;

import java.lang.reflect.Field;

import javax.faces.context.FacesContext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import fr.max.lvdc.bean.AbstractGenericBean;

@Component
public class GetParameterUrlAnnotationProcessor implements BeanPostProcessor {

	@Override
	public Object postProcessAfterInitialization(final Object bean,
			final String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(final Object bean,
			final String beanName) throws BeansException {

		if (bean != null
				&& bean.getClass() != null
				&& bean.getClass().getPackage() != null
				&& bean.getClass()
						.getPackage()
						.getName()
						.contains(
								AbstractGenericBean.class.getPackage()
										.getName())) {

			Field[] vars = bean.getClass().getDeclaredFields();
			for (Field currentField : vars) {

				GetParameterUrl param = currentField
						.getAnnotation(GetParameterUrl.class);

				if (param != null) {
					try {

						final String result = FacesContext.getCurrentInstance()
								.getExternalContext().getRequestParameterMap()
								.get(param.paramName());

						if (!currentField.isAccessible()) {
							currentField.setAccessible(true);
						}

						if (currentField.getType().equals(Integer.class)
								&& result != null) {
							currentField.set(bean, Integer.valueOf(result));
						} else {
							currentField.set(bean, result);
						}

						currentField.setAccessible(false);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		}

		return bean;
	}

}
