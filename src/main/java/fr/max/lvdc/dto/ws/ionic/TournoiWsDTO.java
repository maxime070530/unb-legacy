
package fr.max.lvdc.dto.ws.ionic;

import java.util.Date;
import java.util.List;

public class TournoiWsDTO {

    private String libelleTournoi;

    private String dateTournoi;

    private String dateLimiteInscription;

    private String lieuTournoi;

    private Integer nombreJour;

    private String detail;

    private Integer prixUnTableau;
    private Integer prixDeuxTableau;
    private Integer prixTroisTableau;

    private boolean tourGardois;

    private boolean categorieJeune = false;

    private boolean categorieSenior = false;

    private boolean categorieVeteran = false;

    private boolean disciplineSimple = false;

    private boolean disciplineDouble = false;

    private boolean disciplineMixte = false;

    private List<InscriptionWsDTO> listeInscriptions;

    public String getLibelleTournoi() {
	return libelleTournoi;
    }

    public void setLibelleTournoi(String libelleTournoi) {
	this.libelleTournoi = libelleTournoi;
    }

    public String getDateTournoi() {
	return dateTournoi;
    }

    public void setDateTournoi(String dateTournoi) {
	this.dateTournoi = dateTournoi;
    }

    public String getDateLimiteInscription() {
	return dateLimiteInscription;
    }

    public void setDateLimiteInscription(String dateLimiteInscription) {
	this.dateLimiteInscription = dateLimiteInscription;
    }

    public String getLieuTournoi() {
	return lieuTournoi;
    }

    public void setLieuTournoi(String lieuTournoi) {
	this.lieuTournoi = lieuTournoi;
    }

    public Integer getNombreJour() {
	return nombreJour;
    }

    public void setNombreJour(Integer nombreJour) {
	this.nombreJour = nombreJour;
    }

    public String getDetail() {
	return detail;
    }

    public void setDetail(String detail) {
	this.detail = detail;
    }

    public Integer getPrixUnTableau() {
	return prixUnTableau;
    }

    public void setPrixUnTableau(Integer prixUnTableau) {
	this.prixUnTableau = prixUnTableau;
    }

    public Integer getPrixDeuxTableau() {
	return prixDeuxTableau;
    }

    public void setPrixDeuxTableau(Integer prixDeuxTableau) {
	this.prixDeuxTableau = prixDeuxTableau;
    }

    public Integer getPrixTroisTableau() {
	return prixTroisTableau;
    }

    public void setPrixTroisTableau(Integer prixTroisTableau) {
	this.prixTroisTableau = prixTroisTableau;
    }

    public boolean isTourGardois() {
	return tourGardois;
    }

    public void setTourGardois(boolean tourGardois) {
	this.tourGardois = tourGardois;
    }

    public List<InscriptionWsDTO> getListeInscriptions() {
	return listeInscriptions;
    }

    public void
	    setListeInscriptions(List<InscriptionWsDTO> listeInscriptions) {
	this.listeInscriptions = listeInscriptions;
    }

    public boolean isCategorieJeune() {
	return categorieJeune;
    }

    public void setCategorieJeune(boolean categorieJeune) {
	this.categorieJeune = categorieJeune;
    }

    public boolean isCategorieSenior() {
	return categorieSenior;
    }

    public void setCategorieSenior(boolean categorieSenior) {
	this.categorieSenior = categorieSenior;
    }

    public boolean isCategorieVeteran() {
	return categorieVeteran;
    }

    public void setCategorieVeteran(boolean categorieVeteran) {
	this.categorieVeteran = categorieVeteran;
    }

    public boolean isDisciplineSimple() {
	return disciplineSimple;
    }

    public void setDisciplineSimple(boolean disciplineSimple) {
	this.disciplineSimple = disciplineSimple;
    }

    public boolean isDisciplineDouble() {
	return disciplineDouble;
    }

    public void setDisciplineDouble(boolean disciplineDouble) {
	this.disciplineDouble = disciplineDouble;
    }

    public boolean isDisciplineMixte() {
	return disciplineMixte;
    }

    public void setDisciplineMixte(boolean disciplineMixte) {
	this.disciplineMixte = disciplineMixte;
    }

}
