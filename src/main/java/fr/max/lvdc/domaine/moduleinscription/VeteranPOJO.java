package fr.max.lvdc.domaine.moduleinscription;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

@Entity
@Table(name = "veteran")
public class VeteranPOJO extends AbstractObjetCreaMAJ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6334129888277157530L;

	@NotNull
	private String qualificationVeteran;

	@NotNull
	private Integer classement;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_veteran")
	public Integer getId() {
		return super.getId();
	}

	@Column(name = "qualification_vet")
	public String getQualificationVeteran() {
		return qualificationVeteran;
	}

	public void setQualificationVeteran(String qualificationVeteran) {
		this.qualificationVeteran = qualificationVeteran;
	}

	@Column(name = "classement")
	public Integer getClassement() {
		return classement;
	}

	public void setClassement(Integer classement) {
		this.classement = classement;
	}

	@Transient
	public String getLabel() {

		final StringBuilder result = new StringBuilder();

		if (this.qualificationVeteran != null) {
			result.append(this.qualificationVeteran);
		}

		if (this.classement != null) {
			result.append(this.classement);
		}

		return result.toString();

	}

}
