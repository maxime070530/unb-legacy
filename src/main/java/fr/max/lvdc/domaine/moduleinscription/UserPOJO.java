
package fr.max.lvdc.domaine.moduleinscription;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

@Entity
@Table(name = "util")
public class UserPOJO extends AbstractObjetCreaMAJ implements UserDetails {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @NotNull
    private String nom;

    @NotNull
    private String mdp;

    private JoueurPOJO joueur;

    private List<RolePOJO> listeRoles;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    public Integer getId() {
	return super.getId();
    }

    public UserPOJO() {
	this.joueur = new JoueurPOJO();
    }

    @Column(name = "nom")
    public String getNom() {
	return nom;
    }

    public void setNom(String nom) {
	this.nom = nom;
    }

    @Column(name = "password")
    public String getMdp() {
	return mdp;
    }

    public void setMdp(String mdp) {
	this.mdp = mdp;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "oid_joueur")
    public JoueurPOJO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurPOJO joueur) {
	this.joueur = joueur;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_user",
	    joinColumns = { @JoinColumn(name = "oid_user") },
	    inverseJoinColumns = { @JoinColumn(name = "oid_role") })
    public List<RolePOJO> getListeRoles() {
	return listeRoles;
    }

    public void setListeRoles(List<RolePOJO> listeRoles) {
	this.listeRoles = listeRoles;
    }

    @Override
    @Transient
    public Collection<GrantedAuthority> getAuthorities() {
	final List<GrantedAuthority> grantedAuthorities =
		new ArrayList<GrantedAuthority>();

	for (final RolePOJO currentRole : this.listeRoles) {
	    grantedAuthorities.add(new SimpleGrantedAuthority(
		    "ROLE_" + currentRole.getCode()));
	}
	return grantedAuthorities;
    }

    @Override
    @Transient
    public String getPassword() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    @Transient
    public String getUsername() {
	// TODO Auto-generated method stub
	return nom;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
	// TODO Auto-generated method stub
	return true;
    }

    @Override
    @Transient
    public boolean isEnabled() {
	// TODO Auto-generated method stub
	return true;
    }
}
