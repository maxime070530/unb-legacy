package fr.max.lvdc.domaine.moduleinscription;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import fr.max.lvdc.core.ConstantValidation;
import fr.max.lvdc.domaine.AbstractObjetCreaMAJ;

@Entity
@Table(name = "document")
public class DocumentPOJO extends AbstractObjetCreaMAJ {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	@Size(max = ConstantValidation.MAXSIZE120)
	private String nom;

	@Size(max = ConstantValidation.MAXSIZE255)
	private String description;

	@Digits(integer = ConstantValidation.MAXSIZE12, fraction = ConstantValidation.MAXSIZE0)
	private Integer taille;

	@Size(max = ConstantValidation.MAXSIZE120)
	private String mime;

	@Size(max = ConstantValidation.MAXSIZE6)
	private String extension;

	private byte[] data;
	
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_description_file")
	public Integer getId() {
		return super.getId();
	}



	/**
	 * @return String getNom
	 */
	@Column(name = "nom_fichier")
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom
	 *            String
	 */
	public void setNom(final String nom) {
		this.nom = nom;
	}

	/**
	 * @return String getDescription
	 */
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            String
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * @return Integer getTaille
	 */
	@Column(name = "taille")
	public Integer getTaille() {
		return taille;
	}

	/**
	 * @param taille
	 *            Integer
	 */
	public void setTaille(final Integer taille) {
		this.taille = taille;
	}

	/**
	 * @return String getMime
	 */
	@Column(name = "type_mime")
	public String getMime() {
		return mime;
	}

	/**
	 * @param mime
	 *            String
	 */
	public void setMime(final String mime) {
		this.mime = mime;
	}

	/**
	 * @return String getExtension
	 */
	@Column(name = "extension")
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            String
	 */
	public void setExtension(final String extension) {
		this.extension = extension;
	}
//
//	@ManyToOne
//	@JoinColumn(name = "oid_tournoi")
//	public TournoiPOJO getTournoi() {
//		return tournoi;
//	}
//
//	public void setTournoi(TournoiPOJO tournoi) {
//		this.tournoi = tournoi;
//	}

	@Lob
	@Column(name = "data")
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @return String getFullName
	 */
	@Transient
	public String getFullName() {
		return this.nom + "." + this.extension;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// CHECKSTYLE:OFF
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
		// CHECKSTYLE:ON
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		// CHECKSTYLE:OFF
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		final DocumentPOJO other = (DocumentPOJO) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
		// CHECKSTYLE:ON
	}

}
