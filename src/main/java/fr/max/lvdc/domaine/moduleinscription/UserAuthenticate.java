
package fr.max.lvdc.domaine.moduleinscription;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UserAuthenticate extends User {

    private String nom;

    private JoueurPOJO joueur;

    /**
     * 
     */
    private static final long serialVersionUID = 4169722797051171300L;

    /**
     * @param username
     *            String
     * @param password
     *            String
     * @param enabled
     *            boolean
     * @param accountNonExpired
     *            boolean
     * @param credentialsNonExpired
     *            boolean
     * @param accountNonLocked
     *            boolean
     * @param authorities
     *            Collection<? extends GrantedAuthority>
     */
    public UserAuthenticate(final String username, final String password,
	    final boolean enabled, final boolean accountNonExpired,
	    final boolean credentialsNonExpired,
	    final boolean accountNonLocked,
	    final Collection<? extends GrantedAuthority> authorities,
	    JoueurPOJO joueur) {
	super(username, password, enabled, accountNonExpired,
		credentialsNonExpired, accountNonLocked, authorities);
	this.joueur = joueur;

    }

    public JoueurPOJO getJoueur() {
	return joueur;
    }

    public void setJoueur(JoueurPOJO joueur) {
	this.joueur = joueur;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	// CHECKSTYLE:OFF
	final int prime = 31;
	int result = super.hashCode();
	result = prime * result + ((nom == null) ? 0 : nom.hashCode());
	return result;
	// CHECKSTYLE:ON
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
	// CHECKSTYLE:OFF
	if (this == obj)
	    return true;
	if (!super.equals(obj))
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	final UserAuthenticate other = (UserAuthenticate) obj;
	if (nom == null) {
	    if (other.nom != null)
		return false;
	} else if (!nom.equals(other.nom))
	    return false;
	return true;
	// CHECKSTYLE:ON
    }

}
