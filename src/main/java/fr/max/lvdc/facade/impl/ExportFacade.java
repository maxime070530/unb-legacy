
package fr.max.lvdc.facade.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.facade.api.IExportFacade;

@Service("exportFacade")
public class ExportFacade implements IExportFacade {

    @Override
    public File exportInscriptions(
	    final List<InscriptionPOJO> listeInscriptions,
	    TournoiPOJO tournoi) throws IOException {

	final File temp = File.createTempFile("export_inscriptions"
		+ listeInscriptions.get(0).getTournoi().getLibelleTournoi(),
		".xlsx");
	final XSSFWorkbook book = new XSSFWorkbook();
	final Sheet sheet = book.createSheet("Export");

	int idxRow = 0;

	Row row = sheet.createRow(idxRow++);
	Cell cell = null;

	// fonts
	XSSFFont font = book.createFont();
	font.setFontHeightInPoints((short) 28);
	font.setItalic(true);
	font.setBoldweight(Font.BOLDWEIGHT_BOLD);

	XSSFFont font1 = book.createFont();
	font1.setFontHeightInPoints((short) 20);
	font1.setItalic(true);

	XSSFFont font2 = book.createFont();
	font2.setFontHeightInPoints((short) 12);
	font2.setItalic(true);
	font.setBoldweight(Font.BOLDWEIGHT_BOLD);

	XSSFFont font3 = book.createFont();
	font3.setFontHeightInPoints((short) 12);
	font3.setItalic(true);

	XSSFFont font4 = book.createFont();
	font4.setFontHeightInPoints((short) 11);
	font4.setItalic(false);

	XSSFFont font5 = book.createFont();
	font5.setFontHeightInPoints((short) 8);
	font5.setItalic(false);

	// Styles
	CellStyle cellStyle = book.createCellStyle();
	cellStyle.setFont(font);
	cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

	CellStyle cellStyle1 = book.createCellStyle();
	cellStyle1.setFont(font1);
	cellStyle1.setAlignment(CellStyle.ALIGN_CENTER);

	CellStyle cellStyle2 = book.createCellStyle();
	cellStyle2.setFont(font2);
	cellStyle2.setAlignment(CellStyle.ALIGN_RIGHT);

	CellStyle cellStyle3 = book.createCellStyle();
	cellStyle3.setFont(font3);
	cellStyle3.setAlignment(CellStyle.ALIGN_CENTER);

	CellStyle cellStyle4 = book.createCellStyle();
	cellStyle4.setFont(font4);
	cellStyle4.setAlignment(CellStyle.ALIGN_CENTER);
	cellStyle4.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
	// cellStyle4.setFillForegroundColor(
	// IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
	// cellStyle4.setFillPattern(CellStyle.SOLID_FOREGROUND);

	CellStyle cellStyle5 = book.createCellStyle();
	cellStyle5.setFont(font5);
	cellStyle5.setAlignment(CellStyle.ALIGN_CENTER);

	CellStyle s = book.createCellStyle();
	s.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	s.setFillPattern(CellStyle.SOLID_FOREGROUND);

	// CellStyle leftAngle = book.createCellStyle();
	// cellStyle4.setBorderBottom(CellStyle.BORDER_MEDIUM);
	// cellStyle4.setBorderTop(CellStyle.BORDER_MEDIUM);
	// cellStyle4.setBorderLeft(CellStyle.BORDER_MEDIUM);

	// titre tournoi
	CellRangeAddress cellRangeAddress = new CellRangeAddress(0, 0, 0, 10);
	sheet.addMergedRegion(cellRangeAddress);
	CellRangeAddress cellRangeAddress1 =
		new CellRangeAddress(1, 1, 0, 10);
	sheet.addMergedRegion(cellRangeAddress1);
	CellRangeAddress cellRangeAddress2 =
		new CellRangeAddress(2, 2, 0, 10);
	sheet.addMergedRegion(cellRangeAddress2);

	// infos club
	CellRangeAddress cellRangeAddress3 = new CellRangeAddress(3, 3, 1, 5);
	sheet.addMergedRegion(cellRangeAddress3);
	CellRangeAddress cellRangeAddress4 =
		new CellRangeAddress(3, 3, 7, 10);
	sheet.addMergedRegion(cellRangeAddress4);
	CellRangeAddress cellRangeAddress5 = new CellRangeAddress(4, 4, 1, 5);
	sheet.addMergedRegion(cellRangeAddress5);
	CellRangeAddress cellRangeAddress6 =
		new CellRangeAddress(4, 4, 7, 10);
	sheet.addMergedRegion(cellRangeAddress6);
	CellRangeAddress cellRangeAddress7 = new CellRangeAddress(5, 5, 1, 5);
	sheet.addMergedRegion(cellRangeAddress7);
	CellRangeAddress cellRangeAddress8 =
		new CellRangeAddress(5, 5, 7, 10);
	sheet.addMergedRegion(cellRangeAddress8);

	CellRangeAddress cellRangeAddress9 =
		new CellRangeAddress(6, 6, 0, 10);
	sheet.addMergedRegion(cellRangeAddress9);

	// tableau joueur
	CellRangeAddress cellRangeAddress10 =
		new CellRangeAddress(7, 8, 0, 0);
	sheet.addMergedRegion(cellRangeAddress10);
	CellRangeAddress cellRangeAddress11 =
		new CellRangeAddress(7, 8, 1, 1);
	sheet.addMergedRegion(cellRangeAddress11);
	CellRangeAddress cellRangeAddress12 =
		new CellRangeAddress(7, 8, 2, 2);
	sheet.addMergedRegion(cellRangeAddress12);
	CellRangeAddress cellRangeAddress13 =
		new CellRangeAddress(7, 8, 3, 3);
	sheet.addMergedRegion(cellRangeAddress13);
	CellRangeAddress cellRangeAddress14 =
		new CellRangeAddress(7, 8, 4, 4);
	sheet.addMergedRegion(cellRangeAddress14);
	CellRangeAddress cellRangeAddress15 =
		new CellRangeAddress(7, 8, 5, 5);
	sheet.addMergedRegion(cellRangeAddress15);
	CellRangeAddress cellRangeAddress16 =
		new CellRangeAddress(7, 7, 6, 7);
	sheet.addMergedRegion(cellRangeAddress16);
	CellRangeAddress cellRangeAddress17 =
		new CellRangeAddress(7, 7, 8, 9);
	sheet.addMergedRegion(cellRangeAddress17);
	CellRangeAddress cellRangeAddress18 =
		new CellRangeAddress(7, 8, 10, 10);
	sheet.addMergedRegion(cellRangeAddress18);

	// // bordures
	// short border = CellStyle.BORDER_MEDIUM;
	// RegionUtil.setBorderTop(border, cellRangeAddress10, sheet, book);
	// RegionUtil.setBorderLeft(border, cellRangeAddress10, sheet, book);
	// RegionUtil.setBorderBottom(border, cellRangeAddress10, sheet, book);

	// Tailles Cellules
	sheet.setColumnWidth((short) 0, (short) (20 * 350));
	sheet.setColumnWidth((short) 1, (short) (20 * 190));
	sheet.setColumnWidth((short) 2, (short) (20 * 0));
	sheet.setColumnWidth((short) 3, (short) (20 * 190));
	sheet.setColumnWidth((short) 4, (short) (20 * 190));
	sheet.setColumnWidth((short) 5, (short) (20 * 190));
	sheet.setColumnWidth((short) 6, (short) (20 * 350));
	sheet.setColumnWidth((short) 7, (short) (20 * 190));
	sheet.setColumnWidth((short) 8, (short) (20 * 350));
	sheet.setColumnWidth((short) 9, (short) (20 * 190));
	sheet.setColumnWidth((short) 10, (short) (20 * 190));

	int idx = 0;

	// Libelle tournoi
	cell = row.createCell(idx);
	cell.setCellValue(tournoi.getLibelleTournoi());
	cell.setCellStyle(cellStyle);

	row = sheet.createRow(idxRow++);

	// Date Tournoi
	cell = row.createCell(idx);
	SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
	cell.setCellValue(formater.format(tournoi.getDateTournoi()));
	cell.setCellStyle(cellStyle1);

	row = sheet.createRow(idxRow++);
	row = sheet.createRow(idxRow++);

	cell = row.createCell(0);
	cell.setCellValue("CLUB:");
	cell.setCellStyle(cellStyle2);

	cell = row.createCell(1);
	cell.setCellValue("Unanim'Bad");
	cell.setCellStyle(cellStyle3);

	cell = row.createCell(6);
	cell.setCellValue("NOM DU RESPONSABLE");
	cell.setCellStyle(cellStyle2);

	cell = row.createCell(7);
	cell.setCellValue("Varinard Laura");
	cell.setCellStyle(cellStyle3);

	row = sheet.createRow(idxRow++);

	cell = row.createCell(0);
	cell.setCellValue("SIGLE:");
	cell.setCellStyle(cellStyle2);

	cell = row.createCell(1);
	cell.setCellValue("UNB");
	cell.setCellStyle(cellStyle3);

	cell = row.createCell(6);
	cell.setCellValue("EMAIL:");
	cell.setCellStyle(cellStyle2);

	cell = row.createCell(7);
	cell.setCellValue("unanimbad30@gmail.com");
	cell.setCellStyle(cellStyle3);

	row = sheet.createRow(idxRow++);

	cell = row.createCell(0);
	cell.setCellValue("LIGUE:");
	cell.setCellStyle(cellStyle2);

	cell = row.createCell(1);
	cell.setCellValue("Occitanie");
	cell.setCellStyle(cellStyle3);

	cell = row.createCell(6);
	cell.setCellValue("NUMERO:");
	cell.setCellStyle(cellStyle2);

	cell = row.createCell(7);
	cell.setCellValue("0669034257");
	cell.setCellStyle(cellStyle3);

	row = sheet.createRow(idxRow++);
	row = sheet.createRow(idxRow++);

	cell = row.createCell(0);
	cell.setCellValue("Nom");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(1);
	cell.setCellValue("Sexe");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(3);
	cell.setCellValue("Licence");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(4);
	cell.setCellValue("Classement");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(5);
	cell.setCellValue("Simple");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(6);
	cell.setCellValue("Mixte");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(8);
	cell.setCellValue("Double");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(10);
	cell.setCellValue("Montant");
	cell.setCellStyle(cellStyle4);

	row = sheet.createRow(idxRow++);

	cell = row.createCell(6);
	cell.setCellValue("Partenaire");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(7);
	cell.setCellValue("Club");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(8);
	cell.setCellValue("Partenaire");
	cell.setCellStyle(cellStyle4);

	cell = row.createCell(9);
	cell.setCellValue("Club");
	cell.setCellStyle(cellStyle4);

	for (InscriptionPOJO inscription : listeInscriptions) {

	    idx = 0;

	    row = sheet.createRow(idxRow++);

	    cell = row.createCell(idx++);
	    cell.setCellValue(inscription.getJoueur().getNomJoueur());
	    cell.setCellStyle(cellStyle5);

	    cell = row.createCell(idx++);
	    cell.setCellValue(inscription.getJoueur().getSexeJoueur());
	    cell.setCellStyle(cellStyle5);

	    cell = row.createCell(idx++);
	    cell = row.createCell(idx++);
	    cell.setCellValue(inscription.getJoueur().getLicenceJoueur());
	    cell.setCellStyle(cellStyle5);

	    cell = row.createCell(idx++);
	    cell.setCellValue(inscription.getJoueur().getClassementCompos());
	    cell.setCellStyle(cellStyle5);

	    if (inscription.isCategorieSimple()) {
		cell = row.createCell(idx++);
		cell.setCellValue("OUI");
		cell.setCellStyle(cellStyle5);
	    } else {
		cell = row.createCell(idx++);
		cell.setCellValue("NON");
		cell.setCellStyle(cellStyle5);
	    }

	    if (inscription.isCategorieMixte()) {
		cell = row.createCell(idx++);
		cell.setCellValue(
			inscription.getPartenaireMixte().getNomJoueur());
		cell.setCellStyle(cellStyle5);
	    } else {
		cell = row.createCell(idx++);
		cell.setCellValue("");
		cell.setCellStyle(cellStyle5);
	    }

	    if (inscription.isCategorieMixte()) {
		cell = row.createCell(idx++);
		cell.setCellValue(inscription.getPartenaireMixte().getClub()
			.getAbrevClub());
		cell.setCellStyle(cellStyle5);
	    } else {
		cell = row.createCell(idx++);
		cell.setCellValue("");
		cell.setCellStyle(cellStyle5);
	    }

	    if (inscription.isCategorieDouble()) {
		cell = row.createCell(idx++);
		cell.setCellValue(
			inscription.getPartenaireDouble().getNomJoueur());
		cell.setCellStyle(cellStyle5);
	    } else {
		cell = row.createCell(idx++);
		cell.setCellValue("");
		cell.setCellStyle(cellStyle5);
	    }

	    if (inscription.isCategorieDouble()) {
		cell = row.createCell(idx++);
		cell.setCellValue(inscription.getPartenaireDouble().getClub()
			.getAbrevClub());
		cell.setCellStyle(cellStyle5);
	    } else {
		cell = row.createCell(idx++);
		cell.setCellValue("");
		cell.setCellStyle(cellStyle5);
	    }

	    cell = row.createCell(idx++);
	    cell.setCellValue(inscription.getPrix());
	    cell.setCellStyle(cellStyle5);

	}

	row = sheet.createRow(idxRow++);
	row = sheet.createRow(idxRow++);

	idx = 9;
	int x = 0;
	int y = 0;
	for (InscriptionPOJO i : listeInscriptions) {
	    x = x + i.getPrix();
	    if (!i.isPaye()) {
		y = x + i.getPrix();
	    }
	}
	cell = row.createCell(idx++);

	cell = row.createCell(idx++);
	cell.setCellValue("Total: " + x + " euros");
	cell.setCellStyle(cellStyle5);

	final FileOutputStream os = new FileOutputStream(temp);
	book.write(os);

	// Close workbook, OutputStream and Excel file to prevent leak
	os.close();
	book.close();

	return temp;

    }

    @Override
    public File exportCompta(final List<JoueurPOJO> listeJoueurs)
	    throws IOException {

	final File temp = File.createTempFile("export_compta", ".xlsx");
	final XSSFWorkbook book = new XSSFWorkbook();
	final Sheet sheet = book.createSheet("Export");
	for (int x = 0; x < 14; x++) {
	    sheet.setColumnWidth((short) x, (short) (20 * 256));
	    // sheet.autoSizeColumn(x, true);
	}

	sheet.setColumnWidth((short) 1, (short) (20 * 450));
	sheet.setColumnWidth((short) 3, (short) (20 * 450));

	final XSSFCellStyle styleBold = book.createCellStyle();
	final XSSFFont fontBold = book.createFont();
	fontBold.setBold(true);
	styleBold.setFont(fontBold);

	CellStyle cellStyle = book.createCellStyle();
	cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

	CellStyle celleStyle1 = book.createCellStyle();
	celleStyle1.setAlignment(CellStyle.ALIGN_CENTER);
	celleStyle1.setBorderBottom(CellStyle.BORDER_MEDIUM);
	celleStyle1.setBorderLeft(CellStyle.BORDER_MEDIUM);
	celleStyle1.setBorderRight(CellStyle.BORDER_MEDIUM);
	celleStyle1.setBorderTop(CellStyle.BORDER_MEDIUM);

	int idxRow = 0;

	// EN tete
	Row row = sheet.createRow(idxRow++);

	Cell cell = null;

	int idx = 0;

	// Nom joueur
	cell = row.createCell(idx++);
	cell.setCellValue("Nom Joueur");
	cell.setCellStyle(cellStyle);

	cell = row.createCell(idx++);
	cell.setCellValue("Nom tournoi");
	cell.setCellStyle(cellStyle);

	cell = row.createCell(idx++);
	cell.setCellValue("Nombre tableau");
	cell.setCellStyle(cellStyle);

	cell = row.createCell(idx++);
	cell.setCellValue("Prix");
	cell.setCellStyle(cellStyle);

	cell = row.createCell(idx++);
	cell.setCellValue("Inscription offerte");
	cell.setCellStyle(cellStyle);

	for (JoueurPOJO joueur : listeJoueurs) {

	    row = sheet.createRow(idxRow++);

	    idx = 0;

	    cell = row.createCell(idx++);
	    cell.setCellValue(joueur.getLabel());

	    for (InscriptionPOJO inscription : joueur
		    .getListeInscriptions()) {
		idx = 1;
		row = sheet.createRow(idxRow++);

		cell = row.createCell(idx++);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(
			inscription.getTournoi().getLibelleTournoi());

		cell = row.createCell(idx++);
		cell.setCellStyle(cellStyle);
		cell.setCellValue(inscription.getNbTableau());

		cell = row.createCell(idx++);
		cell.setCellValue(inscription.getPrix());
		cell.setCellStyle(celleStyle1);

		cell = row.createCell(idx++);
		cell.setCellStyle(celleStyle1);
		if (inscription.isPaye()) {
		    cell.setCellValue("OUI");
		} else {
		    cell.setCellValue("NON");
		}

	    }

	    row = sheet.createRow(idxRow++);
	    row = sheet.createRow(idxRow++);

	    idx = 3;

	    cell = row.createCell(idx++);
	    cell.setCellValue(
		    "Total " + joueur.getLabel() + " : " + joueur.getTotal());
	    cell.setCellStyle(celleStyle1);

	}

	final FileOutputStream os = new FileOutputStream(temp);
	book.write(os);

	// Close workbook, OutputStream and Excel file to prevent leak
	os.close();
	book.close();

	return temp;

    }

}
