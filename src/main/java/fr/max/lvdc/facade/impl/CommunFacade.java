
package fr.max.lvdc.facade.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.domaine.moduleinscription.DocumentPOJO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;
import fr.max.lvdc.dto.commun.FeedBackExceptionDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;
import fr.max.lvdc.exception.ValidationException;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.service.api.ICategorieTournoiService;
import fr.max.lvdc.service.api.IClubService;
import fr.max.lvdc.service.api.ICommunService;
import fr.max.lvdc.service.api.IDocumentService;
import fr.max.lvdc.service.api.IInscriptionService;
import fr.max.lvdc.service.api.IJoueurService;
import fr.max.lvdc.service.api.ITournoiService;
import fr.max.lvdc.service.api.IVeteranService;

@Service("communFacade")
public class CommunFacade implements ICommunFacade {

	@SuppressWarnings("unused")
	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(CommunFacade.class);

	// debut injection
	@Autowired
	private ITournoiService tournoiService;
	@Autowired
	private IJoueurService joueurService;
	@Autowired
	private ICategorieTournoiService categorieTournoiService;

	@Autowired
	private IClubService clubService;
	@Autowired
	private IInscriptionService inscriptionService;
	@Autowired
	private IVeteranService veteranService;
	@Autowired
	private IDocumentService documentService;
	@Autowired
	private ICommunService communService;

	// fin injection

	@Override
	public List<TournoiPOJO> findAllTournoi() {
		return this.tournoiService.findAllTournoi();
	}

	@Override
	public TournoiPOJO findTournoi(int idTournoi) {
		return this.tournoiService.findTournoi(idTournoi);
	}

	@Override
	public void deleteTournoi(TournoiPOJO tournoi) {
		this.tournoiService.deleteTournoi(tournoi);
	}

	@Override
	public void saveTournoi(TournoiPOJO tournoi) {
		this.tournoiService.saveTournoi(tournoi);
	}

	@Override
	public List<TournoiPOJO> findAllTournoiByDate() {
		return this.tournoiService.findAllTournoiByDate();
	}

	@Override
	public List<JoueurPOJO> findAllJoueur() {
		return this.joueurService.findAllJoueur();
	}

	@Override
	public JoueurPOJO findJoueur(int idJoueur) {
		return this.joueurService.findJoueur(idJoueur);
	}

	@Override	
	public void deleteJoueur(JoueurPOJO joueur) {
		this.joueurService.deleteJoueur(joueur);
	}

	@Override
	public void saveJoueur(JoueurPOJO joueur) {
		this.joueurService.saveJoueur(joueur);
	}

	@Override
	public VeteranPOJO findVeteran(int idVeteran) {
		return this.veteranService.findVeteran(idVeteran);
	}

	@Override
	public void updateJoueur(JoueurPOJO joueur) {
		this.joueurService.updateJoueur(joueur);
	}

	@Override
	public List<TournoiPOJO> findAllTournoiOrderByDate() {
		return this.tournoiService.findAllTournoiOrderByDate();
	}

	@Override
	public List<CategorieTournoiPOJO> findAllCategorieTournoi() {
		return this.categorieTournoiService.findAllCategTournoi();
	}

	@Override
	public CategorieTournoiPOJO findCategorieTournoi(int idCategorie) {
		return this.categorieTournoiService.findCategorieTournoi(idCategorie);
	}

	@Override
	public List<JoueurPOJO> findJoueurByFiltre(FiltreJoueurDTO filtre) {
		return this.joueurService.findJoueurByFiltre(filtre);
	}

	@Override
	public JoueurPOJO findJoueurEager(Integer idJoueur) {
		return this.joueurService.findJoueurEager(idJoueur);
	}

	@Override
	public List<ClubPOJO> findClubByFiltre(FiltreClubDTO filtre) {
		return this.clubService.findListClubByFiltre(filtre);
	}

	@Override
	public List<ClubPOJO> findAllClub() {
		return this.clubService.findAllClub();
	}

	@Override
	public void saveClub(ClubPOJO club) {
		this.clubService.saveClub(club);
	}

	@Override
	public List<InscriptionPOJO> findAllInscriptions() {
		return this.inscriptionService.findAllInscriptions();
	}

	@Override
	public InscriptionPOJO findInscription(int idInscription) {
		return this.inscriptionService.findInscription(idInscription);
	}

	@Override
	public void deleteInscription(InscriptionPOJO inscription) {
		this.inscriptionService.deleteInscription(inscription);
	}

	@Override
	public void saveOrUpdateInscription(InscriptionPOJO inscription) {

		if (inscription.isCategorieSimple() || inscription.isCategorieDouble() || inscription.isCategorieMixte()) {

			if ((inscription.getNbTableau() < inscription.getTournoi().getNbTableau())
					|| (inscription.getNbTableau() == inscription.getTournoi().getNbTableau())) {

				for (InscriptionPOJO i : inscription.getTournoi().getListeInscriptions()) {

					if (i.getPartenaireDouble() != null) {

						if (inscription.isCategorieDouble()
								&& inscription.getJoueur().equals(i.getPartenaireDouble())) {
							if (!inscription.getPartenaireDouble().equals(i.getJoueur())) {
								throw new ValidationException(new FeedBackExceptionDTO(
										"Vous etes d�j� inscrit dans le tableau double avec une autre personne"));
							}

						}
					}
					if (i.getPartenaireMixte() != null) {
						if (inscription.isCategorieMixte() && inscription.getJoueur().equals(i.getPartenaireMixte())) {
							if (!inscription.getPartenaireMixte().equals(i.getJoueur())) {
								throw new ValidationException(new FeedBackExceptionDTO(
										"Vous etes d�j� inscrit dans le tableau mixte avec une autre personne"));
							}

						}
					}

					if (inscription.getJoueur().equals(i.getJoueur()) && inscription.isCategorieDouble()
							&& i.isCategorieDouble() && !inscription.equals(i)) {
						throw new ValidationException(
								new FeedBackExceptionDTO("Vous etes d�j� inscrit dans le tableau double"));
					}

					if (inscription.getJoueur().equals(i.getJoueur()) && inscription.isCategorieMixte()
							&& i.isCategorieMixte() && !inscription.equals(i)) {
						throw new ValidationException(
								new FeedBackExceptionDTO("Vous etes d�j� inscrit dans le tableau mixte"));
					}

					if (inscription.getPartenaireDouble() != null && i.getPartenaireDouble() != null
							&& !inscription.equals(i)) {
						if (inscription.isCategorieDouble()
								&& ((inscription.getPartenaireDouble().equals(i.getPartenaireDouble())))) {
							throw new ValidationException(
									new FeedBackExceptionDTO("Votre partenaire est d�j� inscrit dans ce tableau"));
						}
					}

					if (inscription.getPartenaireMixte() != null && i.getPartenaireMixte() != null
							&& !inscription.equals(i)) {
						if (inscription.isCategorieMixte()
								&& ((inscription.getPartenaireMixte().equals(i.getPartenaireMixte())))) {
							throw new ValidationException(
									new FeedBackExceptionDTO("Votre partenaire est d�j� inscrit dans ce tableau"));
						}
					}

					if (inscription.isCategorieSimple() && inscription.getJoueur().equals(i.getJoueur())
							&& i.isCategorieSimple() && !inscription.equals(i)) {
						throw new ValidationException(
								new FeedBackExceptionDTO("Votre partenaire est d�j� inscrit dans ce tableau"));
					}

					if (inscription.isCategorieDouble() && inscription.getJoueur().getSexeJoueur().equals("Homme")
							&& !inscription.getPartenaireDouble().getSexeJoueur().equals("Homme")) {
						throw new ValidationException(
								new FeedBackExceptionDTO("Votre partenaire n'est pas du meme sexe"));
					}

					if (inscription.isCategorieDouble() && inscription.getJoueur().getSexeJoueur().equals("Dame")
							&& !inscription.getPartenaireDouble().getSexeJoueur().equals("Dame")) {
						throw new ValidationException(
								new FeedBackExceptionDTO("Votre partenaire n'est pas du meme sexe"));
					}

					if (inscription.isCategorieMixte()) {
						if (inscription.getJoueur().getSexeJoueur().equals("Homme")
								&& !inscription.getPartenaireMixte().getSexeJoueur().equals("Dame")) {
							throw new ValidationException(
									new FeedBackExceptionDTO("Votre partenaire n'est pas du sexe oppos�"));
						}
					}

					if (inscription.isCategorieMixte()) {
						if (inscription.getJoueur().getSexeJoueur().equals("Dame")
								&& !inscription.getPartenaireMixte().getSexeJoueur().equals("Homme")) {
							throw new ValidationException(
									new FeedBackExceptionDTO("Votre partenaire n'est pas du sexe oppos�"));
						}
					}
				}

				this.inscriptionService.saveOrUpdateInscription(inscription);

			} else {
				throw new ValidationException(new FeedBackExceptionDTO("Vous ne pouvez pas faire autant de tableau"));
			}

		} else {
			throw new ValidationException(
					new FeedBackExceptionDTO("Vous devez choisir au moins un tableau pour vous inscrire"));
		}
	}

	@Override
	public void updateInscription(InscriptionPOJO inscription) {
		if (inscription.isCategorieSimple() || inscription.isCategorieDouble() || inscription.isCategorieMixte()) {

			if ((inscription.getNbTableau() < inscription.getTournoi().getNbTableau())
					|| (inscription.getNbTableau() == inscription.getTournoi().getNbTableau())) {
				for (InscriptionPOJO i : inscription.getTournoi().getListeInscriptions()) {

					if (i.getPartenaireDouble() != null) {

						if (inscription.isCategorieDouble()
								&& inscription.getJoueur().equals(i.getPartenaireDouble())) {
							if (!inscription.getPartenaireDouble().equals(i.getJoueur())) {
								throw new ValidationException(new FeedBackExceptionDTO(
										"Vous etes d�j� inscrit dans le tableau double avec une autre personne"));
							}

						}
					}
					if (i.getPartenaireMixte() != null) {
						if (inscription.isCategorieMixte() && inscription.getJoueur().equals(i.getPartenaireMixte())) {
							if (!inscription.getPartenaireMixte().equals(i.getJoueur())) {
								throw new ValidationException(new FeedBackExceptionDTO(
										"Vous etes d�j� inscrit dans le tableau mixte avec une autre personne"));
							}

						}
					}

				}

				this.inscriptionService.updateInscription(inscription);

			} else {
				throw new ValidationException(new FeedBackExceptionDTO("Vous ne pouvez pas faire autant de tableau"));
			}

		} else {
			throw new ValidationException(
					new FeedBackExceptionDTO("Vous devez choisir au moins un tableau pour vous inscrire"));
		}
	}

	@Override
	public List<InscriptionPOJO> findInscriptionByTournoi(int idTournoi) {
		return this.inscriptionService.findInscriptionByTournoi(idTournoi);
	}

	@Override
	public ClubPOJO findClub(int idClub) {
		return this.clubService.findClub(idClub);
	}

	@Override
	public void updateClub(ClubPOJO club) {
		this.clubService.updateClub(club);
	}

	@Override
	public void deleteClub(ClubPOJO club) {
		this.clubService.deleteClub(club);
	}

	@Override
	public ClubPOJO findOneClubByFiltre(FiltreClubDTO filtre) {
		return this.clubService.findOneClubByFiltre(filtre);
	}

	@Override
	public void saveDocument(DocumentPOJO document) {
		this.documentService.saveDocument(document);
	}

	@Override
	public List<DocumentPOJO> findDocumentByTournoi(Integer idTournoi) {
		return this.documentService.findDocumentByTournoi(idTournoi);
	}

	@Override
	public void deleteDocument(DocumentPOJO document) {
		this.documentService.deleteDocument(document);
	}

	@Override
	public List<JoueurPOJO> findListeJoueursWhereNoCompte() {
		return this.joueurService.findListeJoueursWhereNoCompte();
	}

	@Override
	public List<JoueurWsDTO> rechercheListeExterne(String query) {

		List<JoueurWsDTO> listeJoueurs = new ArrayList<JoueurWsDTO>();

		CloseableHttpClient httpclient = HttpClients.createDefault();

		URIBuilder url = null;
		try {
			url = new URIBuilder("https://www.obc-nimes.com/api/joueur");
		} catch (URISyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		url.addParameter("query", query);

		HttpGet getRequest = null;
		try {
			getRequest = new HttpGet(url.build());
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

//	final HttpHost proxy = new HttpHost("proxy.brl.intra", 3128, "http");
//	final RequestConfig config =
//		RequestConfig.custom().setProxy(proxy).build();
//
//	getRequest.setConfig(config);
		getRequest.setHeader("Accept", "application/json");
		getRequest.setHeader("Content-type", "application/json");

		HttpResponse response = null;
		HttpEntity entity = null;

		try {
			response = httpclient.execute(getRequest);
			entity = response.getEntity();
			ObjectMapper mapper = new ObjectMapper();

			JsonNode root = mapper.readTree(entity.getContent());
			httpclient.close();

			List<JoueurWsDTO> joueurs = mapper.readValue(root.traverse(),
					mapper.getTypeFactory().constructCollectionType(List.class, JoueurWsDTO.class));

			for (JoueurWsDTO j : joueurs) {
				listeJoueurs.add(j);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listeJoueurs;
	}

	@Override
	public JoueurPOJO saveJoueurWs(JoueurWsDTO joueurWs) {
		ClubPOJO club = new ClubPOJO();
		FiltreClubDTO filtre = new FiltreClubDTO();
		filtre.setAbrevClub(joueurWs.getClub_court());
		try {
			club = this.clubService.findOneClubByFiltre(filtre);
		} catch (Exception e) {
			club = new ClubPOJO();
			club.setAbrevClub(joueurWs.getClub_court());
			club.setNomClub(joueurWs.getClub());
			this.clubService.saveClub(club);
		}

		JoueurPOJO joueur = new JoueurPOJO();

		joueur.setNomJoueur(joueurWs.getNom());
		joueur.setAgeJoueur(20);

		joueur.setSexeJoueur(joueurWs.getSexe());

		joueur.setClub(club);
		joueur.setLicenceJoueur(joueurWs.getLicence());
		joueur.setExterne(true);

		joueur.setClassementSimple(joueurWs.getClassement_simple());
		joueur.setClassementDouble(joueurWs.getClassement_double());
		joueur.setClassementMixte(joueurWs.getClassement_mixte());

		joueur.setCoteSimple((int) Float.parseFloat(joueurWs.getCote_simple()));
		joueur.setCoteDouble((int) Float.parseFloat(joueurWs.getCote_double()));
		joueur.setCoteMixte((int) Float.parseFloat(joueurWs.getCote_mixte()));

		this.joueurService.saveJoueur(joueur);

		return joueur;

	}

	@Override
	public void refreshClassement(List<JoueurPOJO> listeJoueurs) {

		for (JoueurPOJO joueur : listeJoueurs) {

			List<JoueurWsDTO> listeJoueur = rechercheListeExterne(joueur.getLicenceJoueur());

			if (listeJoueur.size() < 2) {
				for (JoueurWsDTO j : listeJoueur) {
					// if (joueur.getClub() == null) {
					// ClubPOJO club = new ClubPOJO();
					// club.setAbrevClub(j.getClub_court());
					// club.setNomClub(j.getClub());
					// this.clubService.saveClub(club);
					// joueur.setClub(club);
					// }

					ClubPOJO club = new ClubPOJO();
					FiltreClubDTO filtre = new FiltreClubDTO();
					filtre.setAbrevClub(j.getClub_court());
					try {
						club = this.clubService.findOneClubByFiltre(filtre);
					} catch (Exception e) {
						club = new ClubPOJO();
						club.setAbrevClub(j.getClub_court());
						club.setNomClub(j.getClub());
						this.clubService.saveClub(club);
					}

					joueur.setClub(club);
					joueur.setClassementSimple(j.getClassement_simple());
					joueur.setClassementDouble(j.getClassement_double());
					joueur.setClassementMixte(j.getClassement_mixte());
					joueur.setCoteSimple((int) Float.parseFloat(j.getCote_simple()));
					joueur.setCoteDouble((int) Float.parseFloat(j.getCote_double()));
					joueur.setCoteMixte((int) Float.parseFloat(j.getCote_mixte()));
					this.joueurService.saveJoueur(joueur);
				}
			}
		}
	}

	@Override
	public List<InscriptionPOJO> findAllInscriptionsByFiltre(FiltreInscriptionDTO filtre) {
		return this.inscriptionService.findAllInscriptionsByFiltre(filtre);
	}

	@Override
	public JoueurPOJO getJoueurByLicence(String licence) {
		return this.joueurService.getJoueurByLicence(licence);
	}

}
