
package fr.max.lvdc.facade.api;

import java.io.InputStream;
import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.dto.muduleinscription.StatusImportDTO;

public interface IImportFacade {

    boolean importUser(InputStream file,
	    List<List<String>> listeAnomalies, List<UserPOJO> listeUserToSave,
	    StatusImportDTO status);

}
