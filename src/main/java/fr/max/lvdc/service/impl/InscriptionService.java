
package fr.max.lvdc.service.impl;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.IInscriptionDAO;
import fr.max.lvdc.dao.api.IJoueurDAO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;
import fr.max.lvdc.service.api.IInscriptionService;
import fr.max.lvdc.service.api.IJoueurService;

@Service("inscriptionService")
public class InscriptionService implements IInscriptionService {

    @Autowired
    private IInscriptionDAO inscriptionDAO;

    @Override
    public List<InscriptionPOJO> findAllInscriptions() {
	return this.inscriptionDAO.findAll();

    }

    @Override
    public InscriptionPOJO findInscription(int idInscription) {
	return this.inscriptionDAO.get(idInscription);
    }

    @Override
    public void updateInscription(InscriptionPOJO inscription) {
	if (!inscription.isCategorieDouble()) {
	    inscription.setPartenaireDouble(null);
	}

	if (!inscription.isCategorieMixte()) {
	    inscription.setPartenaireMixte(null);
	}

	this.inscriptionDAO.saveOrUpdate(inscription);
    }

    @Override
    public void deleteInscription(InscriptionPOJO inscription) {
	this.inscriptionDAO.delete(inscription);
    }

    @Override
    public void saveOrUpdateInscription(InscriptionPOJO inscription) {

	if (!inscription.isCategorieDouble()) {
	    inscription.setPartenaireDouble(null);
	}

	if (!inscription.isCategorieMixte()) {
	    inscription.setPartenaireMixte(null);
	}

	this.inscriptionDAO.saveOrUpdate(inscription);

    }

    @Override
    public List<InscriptionPOJO> findInscriptionByTournoi(int idTournoi) {
	return this.inscriptionDAO.findlisteInscriptionsByTournoi(idTournoi);
    }

    @Override
    public List<InscriptionPOJO>
	    findAllInscriptionsByFiltre(FiltreInscriptionDTO filtre) {
	return this.inscriptionDAO.findAllInscriptionByFiltre(filtre);
    }

}
