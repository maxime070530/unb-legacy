
package fr.max.lvdc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.max.lvdc.dao.api.IJoueurDAO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.service.api.IJoueurService;
import fr.max.lvdc.service.api.IVeteranService;

@Service("joueurService")
public class JoueurService implements IJoueurService {

    // debut injection
    @Autowired
    private IJoueurDAO joueurDAO;

    @Autowired
    private IVeteranService veteranService;

    // fin injection

    @Override
    public List<JoueurPOJO> findAllJoueur() {
	return this.joueurDAO.findAll();
    }

    @Override
    public JoueurPOJO findJoueur(int idJoueur) {
	return this.joueurDAO.get(idJoueur);
    }

    @Override
    public void deleteJoueur(JoueurPOJO joueur) {
	joueur.setClub(null);
	this.joueurDAO.delete(joueur);
    }

    @Override
    public void saveJoueur(JoueurPOJO joueur) {
	// VeteranPOJO veteran1 = this.veteranService.findVeteran(1);
	// VeteranPOJO veteran2 = this.veteranService.findVeteran(2);
	// VeteranPOJO veteran3 = this.veteranService.findVeteran(3);
	// VeteranPOJO veteran4 = this.veteranService.findVeteran(4);
	// VeteranPOJO veteran5 = this.veteranService.findVeteran(5);
	// VeteranPOJO veteran6 = this.veteranService.findVeteran(6);
	//
	// if (joueur.getAgeJoueur() > 34 && joueur.getAgeJoueur() < 40) {
	// joueur.setVeteran(veteran1);
	// } else if (joueur.getAgeJoueur() >= 40
	// && joueur.getAgeJoueur() < 44) {
	// joueur.setVeteran(veteran2);
	// } else if (joueur.getAgeJoueur() >= 44
	// && joueur.getAgeJoueur() < 49) {
	// joueur.setVeteran(veteran3);
	// } else if (joueur.getAgeJoueur() >= 49
	// && joueur.getAgeJoueur() < 54) {
	// joueur.setVeteran(veteran4);
	// } else if (joueur.getAgeJoueur() >= 54
	// && joueur.getAgeJoueur() < 59) {
	// joueur.setVeteran(veteran5);
	// } else if (joueur.getAgeJoueur() >= 59
	// && joueur.getAgeJoueur() < 64) {
	// joueur.setVeteran(veteran6);
	// } else if (joueur.getAgeJoueur() >= 64) {
	// joueur.setVeteran(veteran6);
	// }

	this.joueurDAO.saveOrUpdate(joueur);
    }

    @Override
    public void updateJoueur(JoueurPOJO joueur) {
	this.joueurDAO.saveOrUpdate(joueur);
    }

    @Override
    public List<JoueurPOJO> findJoueurByFiltre(FiltreJoueurDTO filtre) {

	return this.joueurDAO.findJoueurByFiltre(filtre);

    }

    @Override
    public JoueurPOJO findJoueurEager(Integer idJoueur) {
	return this.joueurDAO.getEager(idJoueur);
    }

    @Override
    public List<JoueurPOJO> findListeJoueursWhereNoCompte() {
	return this.joueurDAO.findlisteJoueursWhereNoCompte();
    }

    @Override
    public JoueurPOJO getJoueurByLicence(String licence) {
	return this.joueurDAO.findJoueurByLicence(licence);
    }
}
