package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;

public interface IVeteranService {

	List<VeteranPOJO> findAll();

	VeteranPOJO findVeteran(int idVeteran);

}
