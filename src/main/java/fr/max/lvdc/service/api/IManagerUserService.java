
package fr.max.lvdc.service.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.RolePOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;

public interface IManagerUserService {

    /**
     * @param username
     *            String
     * @return UserPOJO
     */
    UserPOJO getUserByLogin(String username);

    /**
     * @param utilisateur
     */
    void saveMonCompte(UserPOJO utilisateur);

    List<UserPOJO> findAllUser();

    List<RolePOJO> findAllRole();

    RolePOJO getRole(int idRole);

    UserPOJO findUserByJoueur(Integer idJoueur);

    UserPOJO findUserById(Integer idUtilisateur);

    void deleteUser(UserPOJO user);

}
