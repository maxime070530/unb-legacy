
package fr.max.lvdc.dao.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;

public interface IJoueurDAO extends IGenericDAO<JoueurPOJO, Integer> {

    List<JoueurPOJO> findJoueurByFiltre(FiltreJoueurDTO filtre);

    List<JoueurPOJO> findlisteJoueursWhereNoCompte();

    JoueurPOJO findJoueurByLicence(String licence);

}
