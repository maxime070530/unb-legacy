package fr.max.lvdc.dao.api;

import fr.max.lvdc.domaine.moduleinscription.VeteranPOJO;

public interface IVeteranDAO extends IGenericDAO<VeteranPOJO, Integer> {

}
