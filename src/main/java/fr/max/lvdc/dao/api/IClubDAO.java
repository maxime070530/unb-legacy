package fr.max.lvdc.dao.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.ClubPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreClubDTO;

public interface IClubDAO extends IGenericDAO<ClubPOJO, Integer>{

	List<ClubPOJO> findClubByFiltre(FiltreClubDTO filtre);

	ClubPOJO findOneClubByFiltre(FiltreClubDTO filtre);

}
