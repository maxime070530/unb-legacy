package fr.max.lvdc.dao.api;

import java.util.List;

import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;

public interface IUserDAO extends IGenericDAO<UserPOJO, Integer>{

	UserPOJO findByLogin(String username);

	List<UserPOJO> findAllUsers();

	UserPOJO findUserByJoueur(Integer idJoueur);

}
