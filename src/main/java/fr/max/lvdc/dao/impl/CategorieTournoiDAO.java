package fr.max.lvdc.dao.impl;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.ICategorieTournoiDAO;
import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;

@Repository("categorieTournoiDAO")
public class CategorieTournoiDAO extends
		GenericHibernateDaoImpl<CategorieTournoiPOJO, Integer> implements
		ICategorieTournoiDAO {

}