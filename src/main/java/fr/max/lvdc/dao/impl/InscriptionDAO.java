
package fr.max.lvdc.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IInscriptionDAO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreInscriptionDTO;

@Repository("inscriptionDAO")
public class InscriptionDAO extends GenericHibernateDaoImpl<InscriptionPOJO, Integer> implements IInscriptionDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<InscriptionPOJO> findAllInscriptionByFiltre(final FiltreInscriptionDTO filtre) {

		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQueryWhere = new StringBuilder();

		final StringBuilder hqlQuery = new StringBuilder("  from InscriptionPOJO as inscription ");
		hqlQuery.append(" left join fetch inscription.joueur as joueur ");
		hqlQuery.append(" left join fetch inscription.tournoi as tournoi ");
		hqlQuery.append(" where 1 = 1 ");

		if (filtre.getNomJoueur() != null && !filtre.getNomJoueur().trim().isEmpty()) {
			hqlQuery.append("  and upper(joueur.nomJoueur) like upper(:nomJoueur) ");
			mapParameters.put("nomJoueur", "%" + filtre.getNomJoueur() + "%");
		}

		if (filtre.getLibelleTournoi() != null && !filtre.getLibelleTournoi().trim().isEmpty()) {
			hqlQuery.append("  and upper(tournoi.libelleTournoi) like upper(:libelleTournoi) ");
			mapParameters.put("libelleTournoi", "%" + filtre.getLibelleTournoi() + "%");
		}

		if (filtre.getLicenceJoueur() != null && !filtre.getLicenceJoueur().trim().isEmpty()) {
			hqlQuery.append("  and upper(joueur.licenceJoueur) like upper(:licenceJoueur) ");
			mapParameters.put("licenceJoueur", "%" + filtre.getLicenceJoueur() + "%");
		}

		if (filtre.getLieuTournoi() != null && !filtre.getLieuTournoi().trim().isEmpty()) {
			hqlQuery.append("  and upper(tournoi.lieuTournoi) like upper(:lieuTournoi) ");
			mapParameters.put("lieuTournoi", "%" + filtre.getLieuTournoi() + "%");
		}

		// if (filtre.getDateInscription() != null) {
		// hqlQueryWhere.append(
		// " and inscription.dateInscription = :dateInscription ");
		// mapParameters.put("dateInscription", filtre.getDateInscription());
		// }

		if (filtre.getIntervalle() != null) {

			Date date = new Date();
			Calendar cal = Calendar.getInstance();

			if (filtre.getIntervalle() == 1) {
				hqlQuery.append(" and inscription.dateInscription <= :currentDate ");
			} else if (filtre.getIntervalle() == 2) {
				cal.setTime(new Date());
				cal.add(Calendar.MONTH, -1);
				date = cal.getTime();
				hqlQuery.append(" and inscription.dateInscription >= :oneMonth ");
				hqlQuery.append(" and inscription.dateInscription <= :currentDate ");
				mapParameters.put("oneMonth", date);
			} else if (filtre.getIntervalle() == 3) {
				cal.setTime(new Date());
				cal.add(Calendar.MONTH, -3);
				date = cal.getTime();
				hqlQuery.append(" and inscription.dateInscription >= :threeMonth ");
				hqlQuery.append(" and inscription.dateInscription <= :currentDate ");
				mapParameters.put("threeMonth", date);
			} else if (filtre.getIntervalle() == 4) {
				cal.setTime(new Date());
				cal.add(Calendar.MONTH, -6);
				date = cal.getTime();
				hqlQuery.append(" and inscription.dateInscription >= :sixMonth ");
				hqlQuery.append(" and inscription.dateInscription <= :currentDate ");
				mapParameters.put("sixMonth", date);
			}

			mapParameters.put("currentDate", new Date());

		}

		if (filtre.getDateTournoi() != null) {
			hqlQueryWhere.append("  and  tournoi.dateTournoi = :dateTournoi ");
			mapParameters.put("dateTournoi", filtre.getDateTournoi());
		}

		if (filtre.getPaye() != null) {

			if (filtre.getPaye() == 1) {
				hqlQuery.append(" ");
			} else if (filtre.getPaye() == 2) {
				hqlQuery.append("  and inscription.paye = false");
			} else if (filtre.getPaye() == 3) {
				hqlQuery.append("  and inscription.paye = true");
			}

		}

		if (filtre.getInterne() != null) {

			if (filtre.getInterne()) {
				hqlQuery.append("  and joueur.externe = false ");
			} else {
				hqlQuery.append("  and joueur.externe = true");
			}

		}

		final Query query = em.createQuery(hqlQuery.append(hqlQueryWhere).toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InscriptionPOJO> findlisteInscriptionsByTournoi(final Integer idTournoi) {
		final Map<String, Object> mapParameters = new HashMap<String, Object>();

		final StringBuilder hqlQuery = new StringBuilder(" from InscriptionPOJO as inscription ");
		hqlQuery.append(" left join fetch inscription.tournoi as tournoi ");
		hqlQuery.append(" where 1 = 1 ");
		hqlQuery.append(" and inscription.categorieVeteran = false ");
		if (idTournoi != null) {
			mapParameters.put("idTournoi", idTournoi);
			hqlQuery.append(" and tournoi.id = :idTournoi ");
		}

		final Query query = em.createQuery(hqlQuery.toString());

		if (mapParameters != null && !mapParameters.isEmpty()) {

			for (final Map.Entry<String, Object> mapEntry : mapParameters.entrySet()) {
				query.setParameter(mapEntry.getKey(), mapEntry.getValue());
			}
		}
		return query.getResultList();
	}

}
