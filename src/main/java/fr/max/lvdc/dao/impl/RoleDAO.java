package fr.max.lvdc.dao.impl;

import org.springframework.stereotype.Repository;

import fr.max.lvdc.dao.api.IRoleDAO;
import fr.max.lvdc.domaine.moduleinscription.RolePOJO;

@Repository("roleDAO")
public class RoleDAO extends GenericHibernateDaoImpl<RolePOJO, Integer>
		implements IRoleDAO {

}
