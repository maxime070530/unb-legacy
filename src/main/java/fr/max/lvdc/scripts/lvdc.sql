--DROP SEQUENCE IF EXISTS public.seq_id_categorie_tournoi CASCADE;
--
--CREATE SEQUENCE public.seq_id_categorie_tournoi
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--
--DROP SEQUENCE IF EXISTS public.seq_id_classement CASCADE;
--
--CREATE SEQUENCE public.seq_id_classement
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_club CASCADE;
--
--CREATE SEQUENCE public.seq_id_club
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_inscription CASCADE;
--
--CREATE SEQUENCE public.seq_id_inscription
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_joueur CASCADE;
--
--CREATE SEQUENCE public.seq_id_joueur
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_tournoi CASCADE;
--
--CREATE SEQUENCE public.seq_id_tournoi
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_user CASCADE;
--
--CREATE SEQUENCE public.seq_id_user
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_role CASCADE;
--
--CREATE SEQUENCE public.seq_id_role
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF EXISTS public.seq_id_role_user CASCADE;
--
--CREATE SEQUENCE public.seq_id_role_user
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF exists public.seq_id_veteran CASCADE;
--
--CREATE SEQUENCE public.seq_id_veteran
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
--
--DROP SEQUENCE IF exists public.seq_id_description_file CASCADE;
--
--CREATE SEQUENCE public.seq_id_description_file
--INCREMENT BY 1
--MINVALUE 1
--MAXVALUE 9223372036854775807
--START 1;
DROP SEQUENCE IF EXISTS public.seq_id_categorie_tournoi CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_classement CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_club CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_inscription CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_joueur CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_tournoi CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_user CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_role CASCADE;
DROP SEQUENCE IF EXISTS public.seq_id_role_user CASCADE;
DROP SEQUENCE IF exists public.seq_id_veteran CASCADE;
DROP SEQUENCE IF exists public.seq_id_description_file CASCADE;


DROP TABLE IF EXISTS inscription CASCADE;
DROP TABLE IF EXISTS joueur CASCADE;
DROP TABLE IF EXISTS tournoi CASCADE;
DROP TABLE IF EXISTS club CASCADE;
DROP TABLE IF EXISTS classement CASCADE;
DROP TABLE IF EXISTS regroupement CASCADE;
drop table IF EXISTS serie cascade;
DROP TABLE IF EXISTS util cascade;
DROP TABLE IF EXISTS role cascade;
DROP TABLE IF EXISTS role_user cascade;
DROP TABLE IF EXISTS veteran cascade;
DROP TABLE IF EXISTS categorie_tournoi CASCADE;
DROP TABLE IF EXISTS document CASCADE;


CREATE TABLE veteran (
	id_veteran SERIAL PRIMARY KEY,
	qualification_vet varchar(1) NOT NULL,
	classement int4 NOT NULL
)
WITH (
	OIDS=FALSE
) ;

INSERT INTO public.veteran
(id_veteran, qualification_vet, classement)
VALUES(1, 'V', 1);
INSERT INTO public.veteran
(id_veteran, qualification_vet, classement)
VALUES(2, 'V', 2);
INSERT INTO public.veteran
(id_veteran, qualification_vet, classement)
VALUES(3, 'V', 3);
INSERT INTO public.veteran
(id_veteran, qualification_vet, classement)
VALUES(4, 'V', 4);
INSERT INTO public.veteran
(id_veteran, qualification_vet, classement)
VALUES(5, 'V', 5);
INSERT INTO public.veteran
(id_veteran, qualification_vet, classement)
VALUES(6, 'V', 6);

	
	
CREATE TABLE public.classement (
	id_classement SERIAL PRIMARY KEY,
	qualification_classement varchar(1) NOT NULL,
    classement int4 NOT NULL
	
	
)
WITH (
	OIDS=FALSE
) ;



CREATE TABLE public.club (
	id_club SERIAL PRIMARY KEY,
	nom_club varchar(100) NOT NULL,
	ville_club varchar(100) NULL,
	abrev_club varchar(100) NULL
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public.categorie_tournoi (
	id_categorie_tournoi SERIAL PRIMARY KEY,
	jeune int4 NOT NULL,
	veteran int4 NOT NULL,
	senior int4 NOT NULL,
	discipline_simple int4 NOT NULL,
	discipline_double int4 NOT NULL,
	discipline_mixte int4 NOT NULL,
	classement_max int4 NULL,
	classement_min int4 NULL,
	CONSTRAINT categorie_tournoi_classsement_min_fk FOREIGN KEY (classement_min) REFERENCES public.classement(id_classement),
	CONSTRAINT categorie_tournoi_classsement_max_fk FOREIGN KEY (classement_max) REFERENCES public.classement(id_classement)
)
WITH (
	OIDS=FALSE
) ;



CREATE TABLE public.joueur (
	id_joueur SERIAL PRIMARY KEY,
	nom varchar(100) NOT NULL,
	prenom varchar(100) NOT NULL,
	age int4 NOT NULL,
	sexe varchar(100) NOT NULL,
	licence varchar(100) NOT NULL,
	oid_club int4 NOT NULL DEFAULT 1,
	oid_veteran int4 NULL,
	classement_simple int4 NULL,
	classement_double int4 NULL,
	classement_mixte int4 NULL,
	externe int4 NULL,
	CONSTRAINT joueur_club_fk FOREIGN KEY (oid_club) REFERENCES public.club(id_club),
	CONSTRAINT joueur_classement_simple_fk FOREIGN KEY (classement_simple) REFERENCES public.classement(id_classement),
	CONSTRAINT joueur_classement_double_fk FOREIGN KEY (classement_double) REFERENCES public.classement(id_classement),
	CONSTRAINT joueur_classement_mixte_fk FOREIGN KEY (classement_mixte) REFERENCES public.classement(id_classement),
	CONSTRAINT joueur_vetaran_fk FOREIGN KEY (oid_veteran) REFERENCES public.veteran(id_veteran)
)
WITH (
	OIDS=FALSE
) ;



CREATE TABLE public.tournoi (
	id_tournoi SERIAL PRIMARY KEY,
	libelle_tournoi varchar(100) NULL,
	date_tournoi DATE NOT NULL,
	date_limite_inscription DATE NOT NULL,
	nombre_jour int4 NOT NULL,
	prix_un_tableau int4 NOT NULL,
	prix_deux_tableau int4 NULL,
	prix_trois_tableau int4 NULL,
	lien varchar(100) NULL,
	-- email varchar(50) NOT NULL,
	lieu_tournoi varchar(100) NOT NULL,
	detail varchar(10000000) NULL,
	-- adresse_tournoi varchar(100) NULL,
	oid_club int4 NULL,
	oid_categorie_tournoi int4 NULL,
	CONSTRAINT tournoi_categorie_tournoi_fk FOREIGN KEY (oid_categorie_tournoi) REFERENCES public.categorie_tournoi(id_categorie_tournoi),
	CONSTRAINT tournoi_club_fk FOREIGN KEY (oid_club) REFERENCES public.club(id_club)
	
	
)
WITH (
	OIDS=FALSE
) ;

ALTER TABLE tournoi add column N int4;
ALTER TABLE tournoi add column R int4;
ALTER TABLE tournoi add column D int4;
ALTER TABLE tournoi add column P int4;

CREATE TABLE document(
	id_description_file SERIAL PRIMARY KEY, 
	nom_fichier varchar(100) NULL,
	description varchar(100) NULL,
	taille int4 NULL,
	type_mime varchar(100) NULL,
	extension varchar(100) NULL,
	data oid NULL,
	oid_tournoi int4 NULL,
	CONSTRAINT doc_tournoi_fk FOREIGN KEY (oid_tournoi) REFERENCES public.tournoi(id_tournoi)
	
)
WITH (
	OIDS=FALSE
) ;

-- table jointure -- 
CREATE TABLE public.inscription (
	id_inscription SERIAL PRIMARY KEY,
	oid_tournoi int4 NULL,
	oid_joueur int4 NULL,
	categorie_simple int4 NULL,
	categorie_double int4 NULL,
	categorie_mixte int4 NULL,
	categorie_veteran int4 NULL,
	partenaire_double int4 NULL,
	partenaire_mixte int4 NULL,
	classement_simple int4 NULL,
	classement_double int4 NULL,
	classement_mixte int4 NULL,
	veteran_simple int4 NULL,
	veteran_double int4 NULL,
	date_inscription date NOT NULL,
	veteran_mixte int4 NULL,
	jeune_simple int4 NULL,
	jeune_double int4 NULL,
	jeune_mixte int4 NULL,
	partenaire_externe_double int4 NULL,
	partenaire_externe_mixte int4 NULL,
	CONSTRAINT inscription_joueur_fk FOREIGN KEY (oid_joueur) REFERENCES public.joueur(id_joueur),
	CONSTRAINT inscription_tournoi_fk FOREIGN KEY (oid_tournoi) REFERENCES public.tournoi(id_tournoi),
	CONSTRAINT inscription_partd_fk FOREIGN KEY (partenaire_double) REFERENCES public.joueur(id_joueur),
	CONSTRAINT inscription_partm_fk FOREIGN KEY (partenaire_mixte) REFERENCES public.joueur(id_joueur),
	CONSTRAINT inscr_classement_simple_fk FOREIGN KEY (classement_simple) REFERENCES public.classement(id_classement),
	CONSTRAINT inscr_classement_double_fk FOREIGN KEY (classement_double) REFERENCES public.classement(id_classement),
	CONSTRAINT inscr_classement_mixte_fk FOREIGN KEY (classement_mixte) REFERENCES public.classement(id_classement),
	CONSTRAINT inscr_veteran_simple_fk FOREIGN KEY (veteran_simple) REFERENCES public.veteran(id_veteran),
	CONSTRAINT inscr_veteran_double_fk FOREIGN KEY (veteran_double) REFERENCES public.veteran(id_veteran),
	CONSTRAINT inscr_veteran_mixte_fk FOREIGN KEY (veteran_mixte) REFERENCES public.veteran(id_veteran)
)
WITH (
	OIDS=FALSE
) ;




CREATE TABLE util (
	id_user SERIAL PRIMARY KEY,
	nom varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	oid_joueur int4 NULL,
	CONSTRAINT user_fk FOREIGN KEY (oid_joueur) REFERENCES public.joueur(id_joueur)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE public."role" (
	id_role SERIAL PRIMARY KEY,
	libelle varchar(100) NOT NULL,
	code varchar(100) NOT NULL
)
WITH (
	OIDS=FALSE
) ;


CREATE TABLE public.role_user (
	id_role_user SERIAL PRIMARY KEY,
	oid_role int4 NOT NULL,
	oid_user int4 NOT NULL,
	CONSTRAINT role_fk FOREIGN KEY (oid_role) REFERENCES public."role"(id_role),
	CONSTRAINT user_fk FOREIGN KEY (oid_user) REFERENCES util(id_user)
)
WITH (
	OIDS=FALSE
) ;



INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(1, 'P', 12);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(2, 'P', 11);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(3, 'P', 10);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(4, 'D', 9);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(5, 'D', 8);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(6, 'D', 7);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(7, 'R', 6);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(8, 'R', 5);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(9, 'R', 4);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(10, 'N', 3);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(11, 'N', 2);
INSERT INTO public.classement
(id_classement, qualification_classement, classement)
VALUES(12, 'N', 1);

INSERT INTO public.club
(id_club, nom_club, ville_club, abrev_club)
VALUES (1, 'Les volants de Calvisson', 'Calvisson', 'LVDC');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (1, 'Administrateur', 'ADMI');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (2, 'Joueur', 'JOU');

INSERT INTO public.role
(id_role, libelle, code)
VALUES (3, 'Visiteur', 'VIS');

insert into public.util
(id_user, nom, password)
values (1, 'IFA', 'IFA01');

insert into public.role_user
(id_role_user, oid_role, oid_user)
values(1, 1, 1);

COPY club(nom_club,ville_club, abrev_club) 
FROM 'C:\listeClubs.csv' DELIMITER ';' CSV HEADER;

COPY joueur(nom, prenom, age, sexe, licence, oid_club, externe)
FROM 'C:\listeJoueurs.csv' DELIMITER ';' CSV HEADER;





