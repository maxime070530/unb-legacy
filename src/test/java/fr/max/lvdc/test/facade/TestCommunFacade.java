
package fr.max.lvdc.test.facade;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.max.lvdc.domaine.moduleinscription.CategorieTournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.InscriptionPOJO;
import fr.max.lvdc.domaine.moduleinscription.JoueurPOJO;
import fr.max.lvdc.domaine.moduleinscription.TournoiPOJO;
import fr.max.lvdc.domaine.moduleinscription.UserPOJO;
import fr.max.lvdc.dto.muduleinscription.FiltreJoueurDTO;
import fr.max.lvdc.dto.ws.JoueurWsDTO;
import fr.max.lvdc.facade.api.ICommunFacade;
import fr.max.lvdc.facade.api.IUserManagerFacade;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
public class TestCommunFacade {

    @Autowired
    private ICommunFacade communFacade;

    @Autowired
    private IUserManagerFacade userFacade;

    private final static String URL_API_REST =
	    "https://www.obc-nimes.com/api/";

    @Test
    @Rollback(true)
    @Transactional(propagation = Propagation.REQUIRED)
    public void testfindJoueurByFiltre() {

	final FiltreJoueurDTO filtre = new FiltreJoueurDTO();
	filtre.setNomJoueur("Grau");
	filtre.setLicenceJoueur("06852329");

	List<JoueurPOJO> listeJoueurs =
		this.communFacade.findJoueurByFiltre(filtre);

	if (listeJoueurs == null || listeJoueurs.isEmpty()) {
	    fail("ne peut pas etre null");
	}

	assertTrue(true);

    }

    @Test
    @Rollback(true)
    @Transactional(propagation = Propagation.REQUIRED)
    public void testfindAllUsers() {
	List<UserPOJO> listeUsers = this.userFacade.findAllUsers();

	if (listeUsers == null || listeUsers.isEmpty()) {
	    fail("");
	}

	assertTrue(true);

    }

    @Test
    public void changeMDP() {
	UserPOJO user = this.userFacade.findUserById(1);

	user.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt("IFA01"));
	this.userFacade.saveMonCompte(user);
    
    }
    
    @Test
    public void createUser() {
	UserPOJO user = new UserPOJO();
	user.setNom("IFA02");
	
	
	user.setMdp(fr.max.lvdc.core.Utils.encodeBCrypt("IFA01"));
	this.userFacade.saveMonCompte(user);
    
    }

    @Test
    @Rollback(true)
    @Transactional(propagation = Propagation.REQUIRED)
    public void testFindAllTournoi() {
	List<TournoiPOJO> listeTournois = this.communFacade.findAllTournoi();

	if (listeTournois == null || listeTournois.isEmpty()) {
	    fail("impossible");
	}

	assertTrue(true);
    }

    @Test
    @Rollback(true)
    @Transactional(propagation = Propagation.REQUIRED)
    public void testFindAllUsers() {
	List<UserPOJO> listeUsers = this.userFacade.findAllUsers();

	if (listeUsers == null || listeUsers.isEmpty()) {
	    fail("impossible");
	}

	assertTrue(true);

    }
    

    @Test
    @Rollback(true)
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteInscription() {
	List<InscriptionPOJO> listeInscr =
		this.communFacade.findAllInscriptions();

	try {
	    this.communFacade.deleteInscription(listeInscr.get(0));
	} catch (Exception e) {
	    fail();
	}

	assertTrue(true);

    }

    @Test
    public void testSimulateur() throws URISyntaxException {

	CloseableHttpClient httpclient = HttpClients.createDefault();

	URIBuilder url = null;
	url = new URIBuilder(URL_API_REST + "joueur");
	url.addParameter("query", "06852329");

	final HttpHost proxy = new HttpHost("proxy.brl.intra", 3128, "http");
	final RequestConfig config =
		RequestConfig.custom().setProxy(proxy).build();

	final HttpGet getRequest = new HttpGet(url.build());
	getRequest.setConfig(config);
	getRequest.setHeader("Accept", "application/json");
	getRequest.setHeader("Content-type", "application/json");

	HttpResponse response = null;
	HttpEntity entity = null;

	try {
	    response = httpclient.execute(getRequest);
	    entity = response.getEntity();
	    ObjectMapper mapper = new ObjectMapper();

	    JsonNode root = mapper.readTree(entity.getContent());
	    httpclient.close();

	    List<JoueurWsDTO> joueurs = mapper.readValue(root.traverse(),
		    mapper.getTypeFactory().constructCollectionType(
			    List.class, JoueurWsDTO.class));

	    for (JoueurWsDTO j : joueurs) {
		System.out.println(j.getNom());
	    }

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    @Test
    public void testSaveTournoi() {
	TournoiPOJO tournoi = new TournoiPOJO();
	CategorieTournoiPOJO categ =
		this.communFacade.findCategorieTournoi(1);
	tournoi.setCategorieTournoi(categ);
	tournoi.setD(true);
	tournoi.setN(true);
	tournoi.setDateLimiteInscription(new Date());
	tournoi.setP(true);
	tournoi.setDetail(URL_API_REST);
	tournoi.setDateTournoi(new Date());
	tournoi.setLieuTournoi("aa");
	tournoi.setLibelleTournoi("test");
	tournoi.setPrixUnTableau(12);
	tournoi.setNombreJour(2);
	this.communFacade.saveTournoi(tournoi);
    }

    @Test
    public void refreshClassement() {

	List<JoueurPOJO> listeJoueur = this.communFacade.findAllJoueur();
	List<JoueurWsDTO> listeJoueurWs = new ArrayList<JoueurWsDTO>();
	for (JoueurPOJO joueur : listeJoueur) {

	    listeJoueurWs = this.communFacade
		    .rechercheListeExterne(joueur.getLicenceJoueur());

	    if (listeJoueurWs.size() < 2) {
		for (JoueurWsDTO j : listeJoueurWs) {
		    if (j.getLicence() != null) {
			joueur.setClassementSimple(j.getClassement_simple());
			joueur.setClassementDouble(j.getClassement_double());
			joueur.setClassementMixte(j.getClassement_mixte());
			joueur.setCoteSimple(
				(int) Float.parseFloat(j.getCote_simple()));
			joueur.setCoteDouble(
				(int) Float.parseFloat(j.getCote_double()));
			joueur.setCoteMixte(
				(int) Float.parseFloat(j.getCote_mixte()));
			this.communFacade.saveJoueur(joueur);
		    }
		}
	    }
	}

    }

    @Test
    public void refreshTournoiPaye() {
	FiltreJoueurDTO filtre = new FiltreJoueurDTO();
	filtre.setInterne(true);
	List<JoueurPOJO> listeJoueurs =
		this.communFacade.findJoueurByFiltre(filtre);

	for (JoueurPOJO joueur : listeJoueurs) {

	    if (joueur.getClassementSimple().contains("P")
		    || joueur.getClassementDouble().contains("P")
		    || joueur.getClassementMixte().contains("P")) {
		joueur.setTourGardoisPaye(3);
	    }

	    if (joueur.getClassementSimple().contains("D")
		    || joueur.getClassementDouble().contains("D")
		    || joueur.getClassementMixte().contains("D")) {
		joueur.setTournoiPaye(1);
		joueur.setTourGardoisPaye(0);
	    }

	    if (joueur.getClassementSimple().contains("R")
		    || joueur.getClassementDouble().contains("R")
		    || joueur.getClassementMixte().contains("R")) {
		joueur.setTournoiPaye(3);
		joueur.setTourGardoisPaye(0);
	    }

	    if (joueur.getClassementSimple().contains("N")
		    || joueur.getClassementDouble().contains("N")
		    || joueur.getClassementMixte().contains("N")) {
		joueur.setTournoiPaye(5);
		joueur.setTourGardoisPaye(0);
	    }

	    this.communFacade.saveJoueur(joueur);

	}
    }

    @Test
    public void testDate() {
	Calendar cal = Calendar.getInstance();
	cal.setTime(new Date());
	System.out.println(cal.get(Calendar.DAY_OF_WEEK));
    }

}
